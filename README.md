# Инициализированный проект Laravel + Redis + Postgres

#### Зависимости:

- PHP7.4
- Redis:alpine
- PostgreSQL:alpine
- Composer:latest
- Codeception:1.0.0

#### Инструкция по развертыванию:
```
cp .env.example .env
docker-compose up -d --build
docker exec -it {PROJECT_NAME} /bin/bash
composer install
./artisan key:generate
./artisan migrate
```
*{PROJECT_NAME} - значение в .env*